//
//  SelectionStrategy.swift
//  Swiftvolution
//
//  Created by Jeremy Pereira on 05/09/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//
import Toolbox

typealias Selector<T> = ([EvaluatedCandidate<T>], Bool, Int, PRNG) -> [T]


/// Some built in selection strategies
public enum SelectionStrategy<T>
{

    /// The roulette wheel strategy. Conceptually a wheel is built with each
    /// candidate having a sector in proportion to its score. A random number is
    /// generated and the candidate whose sector it falls in is eselected.
    ///
    /// - Parameters:
    ///   - candidates: List of candidates with scores.
    ///   - isNatural: if true a higher score is better. With roulette wheel, non
    ///                natural is illegal.
    ///   - count: Number of candidates to generate
    ///   - rng: Random number generator to use
    /// - Returns: A list of selected candidates
    public static func rouletteWheel(candidates: [EvaluatedCandidate<T>], isNatural: Bool, count: Int, rng: PRNG) -> [T]
    {
        precondition(isNatural, "Roulette wheel does not support non natural scores")
        precondition(candidates.count > 0, "Roulette wheel must have more than zero candidates to choose from")

        var cumulativeScore: [(Double, T)] = []
        // Make the wheel
        for candidate in candidates
        {
            if let previousTotal = cumulativeScore.last
            {
                cumulativeScore.append((candidate.score + previousTotal.0, candidate.candidate))
            }
            else
            {
                cumulativeScore.append((candidate.score, candidate.candidate))
            }
        }
		let upperBound = cumulativeScore.last!.0
		let rngRange = rng.maxRange.upperBound - rng.maxRange.lowerBound
        var ret: [T] = []
        // TODO: Can optimise with a binary search algorithm
        while ret.count < count
        {
            let random = Double(rng.zeroBasedRandom(rngRange)) * upperBound / Double(rngRange)
			if let element = cumulativeScore.first(where: { $0.0 > random })
            {
				ret.append(element.1)
            }
        }
        return ret
    }
}
