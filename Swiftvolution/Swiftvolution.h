//
//  Swiftvolution.h
//  Swiftvolution
//
//  Created by Jeremy Pereira on 05/08/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for Swiftvolution.
FOUNDATION_EXPORT double SwiftvolutionVersionNumber;

//! Project version string for Swiftvolution.
FOUNDATION_EXPORT const unsigned char SwiftvolutionVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Swiftvolution/PublicHeader.h>


