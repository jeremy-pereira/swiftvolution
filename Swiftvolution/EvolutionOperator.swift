//
//  EvolutionOperator.swift
//  Swiftvolution
//
//  Created by Jeremy Pereira on 04/09/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import Toolbox


/// Evolution operator is a function that takes a list of individuals and 
/// returns a new list of individuals. Also needs a random number generator
public typealias EvolutionOperator<T, U> = (T, PRNG) -> U


/// Effect a crossover at one point in the two arrays. 
///
/// - Parameters:
///   - a: The first array
///   - b: The second array
///   - rng: The random number generator
/// - Returns: A tuple containing the mirror images of the crossovers
public func simpleCrossover<T>(a: [T], b: [T], rng: PRNG) -> ([T], [T])
{
    let maxCrossOver = min(a.count, b.count)
    let crossOverPoint = rng.zeroBasedRandom(maxCrossOver + 1)
    var child1: [T]
    var child2: [T]
    switch crossOverPoint
    {
    case 0:
        child2 = a
        child1 = b
    default:
        child1 = Array(b[0 ..< crossOverPoint])
        child2 = Array(a[0 ..< crossOverPoint])
        if a.count > crossOverPoint
        {
            child2.append(contentsOf: a[crossOverPoint ..< a.count])
        }
        if b.count > crossOverPoint
        {
            child1.append(contentsOf: b[crossOverPoint ..< b.count])
        }
    }
    return (child1, child2)
}


/// Function that may or may not apply a single mutation to an array of objects
///
/// - Parameters:
///   - gene: The gene that will be mutated
///   - mutations: Set of things the mutation can be
///   - probability: Probability that a mutation will occur
///   - rng: Random number generator
/// - Returns: Either gene unchanged or a mutated version of the gene
public func singleMutation<T>(gene: [T], mutations: [T], probability: Double, rng: PRNG) -> [T]
{
    guard rng.select(probability) else { return gene } // No mutation

    var child = gene
    let mutatedIndex = rng.zeroBasedRandom(child.count)
    child[mutatedIndex] = mutations[rng.zeroBasedRandom(mutations.count)]
    return child
}

/// Struct that encapsulates simple string crossover operations. All parents
/// are mated with each other. Each mating produces two offspring
public struct StringCrossover
{
    // TODO: Multiple crossover points

    public func apply(parents: [String], rng: PRNG) -> [String]
    {
        var ret: [String] = []
        for i in 0 ..< parents.count
        {
            for j in 0 ..< parents.count
            {
				let parent1 = Array(parents[i])
                let parent2 = Array(parents[j])
                let (child1, child2) = simpleCrossover(a: parent1, b: parent2, rng: rng)
                ret.append(String(child1))
                ret.append(String(child2))
            }
        }
        return ret
    }
}

public struct ArrayMutation<T>
{
    private let probability: Double
    private let mutations: [T]

    public init(probability: Double, mutations: [T])
    {
        self.probability = probability
        self.mutations = mutations
    }

    public func apply(parents: [[T]], rng: PRNG) -> [[T]]
    {
        return parents.map
        {
            (parent) -> [T] in
            return singleMutation(gene: parent, mutations: mutations, probability: probability, rng: rng)
        }
    }
}

public struct StringMutation
{
    private let arrayMutation: ArrayMutation<Character>

    public init(probability: Double, characters: [Character])
    {
        self.arrayMutation = ArrayMutation(probability: probability, mutations: characters)
    }

    public func apply(parents: [String], rng: PRNG) -> [String]
    {
        return arrayMutation.apply(parents: parents.map{ Array($0) }, rng: rng).map { String($0) }
    }
}


/// Pipe operation builds a pipeline of `EvolutionOperator`s. All operators will 
/// use the same random number generator
///
/// - Parameters:
///   - a: first function in the pipeline.
///   - b: second function in the pipeline
/// - Returns: The composition of `a` and `b`.
public func | <T, U, V> (a: @escaping EvolutionOperator<T, U>,
                         b: @escaping EvolutionOperator<U, V>) -> EvolutionOperator<T, V>
{

    return { (list, rng) in b(a(list, rng), rng) }
}
