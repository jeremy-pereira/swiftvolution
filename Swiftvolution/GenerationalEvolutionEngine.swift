//
//  GenerationalEvolutionEngine.swift
//  Swiftvolution
//
//  Created by Jeremy Pereira on 04/09/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import Toolbox

var foo: Array<String> = []


/// A generational evolution engine.
/// The candidate factory type determines the EvolveType. i.e. it is whatever
/// type the candidate factory gives us.
public struct GenerationalEvolutionEngine<CF: CandidateFactory>: EvolutionEngine
{
    public typealias EvolveType = CF.ProduceType

    private let candidateFactory: CF
    private let evolutionOperator: EvolutionOperator<[EvolveType], [EvolveType]>
    private let fitnessEvaluator: (EvolveType, [EvolveType]) -> Double
    private let isNatural: Bool
    private let selectionStrategy: ([EvaluatedCandidate<EvolveType>], Bool, Int, PRNG) -> [EvolveType]
    private let rng: PRNG

    /// Create a generational evolution engine.
    ///
    /// - Parameters:
    ///   - candidateFactory: Factory for creating new random individuals
    ///   - evolutionOperator: Function that transforms one population into 
    ///                        another for the next generation, this is the breeding part.    
    ///   - fitnessEvaluator: Function that takes an individual and evaluates its fitness.
    ///                       The population is also passed in for competition evaluations.
    ///                       The returned value must be positive.
    ///   - isNatural: true if a high score is good, false if a low score is good.
    ///   - selectionStrategy: A function that selects individuals. The passed in 
    ///                        array will be sorted. It also takes a bool which 
    ///                        is true for natural order, an int which is the 
    ///                        number of selections to make and a random number 
    ///                        generator.
    ///   - rng: Random number generator.
    public init(candidateFactory: CF,
               evolutionOperator: @escaping EvolutionOperator<[EvolveType], [EvolveType]>,
                fitnessEvaluator: @escaping (EvolveType, [EvolveType]) -> Double,
                       isNatural: Bool = true,
               selectionStrategy: @escaping ([EvaluatedCandidate<EvolveType>], Bool, Int, PRNG) -> [EvolveType],
                			 rng: PRNG)
    {
		self.candidateFactory = candidateFactory
        self.evolutionOperator = evolutionOperator
        self.fitnessEvaluator = fitnessEvaluator
        self.isNatural = isNatural
        self.selectionStrategy = selectionStrategy
        self.rng = rng
    }

    public typealias PopulationDataFactory = ([EvaluatedCandidate<EvolveType>], Bool, Int, Int) -> PopulationData<EvolveType>


    /// Factory for creating population data analysers. You can replace this to
    /// produce a subtype of PopulationDataFactory
    public var populationDataFactory: PopulationDataFactory = {
        (evaluatedCandidates, isNatural, eliteCount, generationNumber) in
        return PopulationData(evaluatedPopulation: evaluatedCandidates,
                              			isNatural: isNatural,
                                       eliteCount: eliteCount,
                                 generationNumber: generationNumber)
    }


    public func evolve(populationCount: Int, eliteCount: Int, seeds: [EvolveType], terminator: TerminationCondition<EvolveType>) -> EvolveType?
    {
        let population = candidateFactory.generateInitialPopulation(count: populationCount, seeds: seeds, rng: rng)
        var evaluatedCandidates = evaluate(population: population)
        var generationNumber = 0
        var stats = PopulationData(evaluatedPopulation: evaluatedCandidates, isNatural: isNatural, eliteCount: eliteCount, generationNumber: 0)
        while evaluatedCandidates.count > 0 && !terminator(stats)
        {
            for observer in observers
            {
                observer(stats)
            }
            generationNumber += 1
			let parents = selectionStrategy(evaluatedCandidates, isNatural, populationCount, rng)
            let children = evolutionOperator(parents, rng)
            
			evaluatedCandidates = evaluate(population: children)
            stats = populationDataFactory(evaluatedCandidates, isNatural, eliteCount, generationNumber)
        }
        return stats.bestCandidate
    }

    private func evaluate(population: [EvolveType]) -> [EvaluatedCandidate<EvolveType>]
    {
        return population
            .map{ EvaluatedCandidate(candidate: $0, score: self.fitnessEvaluator($0, population)) }
            .sorted(by: { (a, b) -> Bool in isNatural ? a.score > b.score : a.score < b.score })
    }

    private var observers: [EvolutionObserver<EvolveType>] = []

    public mutating func addObserver(observer: @escaping EvolutionObserver<EvolveType>)
    {
        observers.append(observer)
    }
}
