//
//  EvolutionEngine.swift
//  Swiftvolution
//
//  Created by Jeremy Pereira on 04/09/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//
import Toolbox


/// Function that takes population data to return a boolean as to whether
/// evolution should terminate
public typealias TerminationCondition<T> = (PopulationData<T>) -> Bool
/// Evolution observer function to be called each time a PopulationData is
/// generatied
public typealias EvolutionObserver<T> = (PopulationData<T>) -> ()

/// API for an evolution engine.
public protocol EvolutionEngine
{
    /// The type of thing that the engine will be evolving.
    associatedtype EvolveType


    /// Run an evolution simulation
    ///
    /// - Parameters:
    ///   - populationCount: Number of individuals in the population
    ///   - eliteCount: Number of elite individuals
    ///   - seeds: List of seed individuals
    ///   - terminator: Conditions under which evolution will stop
    /// - Returns: The fittest individual when evolution stops or nil if the 
    ///            popultion went extinct
    func evolve(populationCount: Int,
                     eliteCount: Int,
                          seeds: [EvolveType],
                     terminator: TerminationCondition<EvolveType>) -> EvolveType?


    /// Add an observer. The observer will b e called each time a new set of
    /// population data is compiled.
    ///
    /// - Parameter observer: The observer to be called.
    mutating func addObserver(observer: @escaping EvolutionObserver<EvolveType>)
}

/// Type of object that can produce things to go in a population
public protocol CandidateFactory
{
    /// The type of thing to produce
    associatedtype ProduceType


    /// Generate a single random candidate
    ///
    /// - Parameter rng: The random number generator to use.
    /// - Returns: A random candidate
    func generateRandomCandidate(rng: PRNG) -> ProduceType

}

public extension CandidateFactory
{

    /// Create a random population possibly using a seed population. If there are
    /// more seeds than we need for the whole population, use the first `count` of them.
    ///
    /// - Parameters:
    ///   - count: Number of individuals in the population.
    ///   - seeds: Preselected seed individuals
    ///   - rng: Random number generator
    /// - Returns: An array of `ProduceType` to act as the population.
    public func generateInitialPopulation(count: Int, seeds: [ProduceType] = [], rng: PRNG) -> [ProduceType]
    {
		guard seeds.count < count
        else
        {
            return Array(seeds[0 ..< count])
        }
        var ret: [ProduceType] = seeds
		while ret.count < count
        {
            ret.append(generateRandomCandidate(rng: rng))
        }
        return ret
    }
}

public struct EvaluatedCandidate<T>: Comparable
{
    public let candidate: T
    public let score: Double

    public init(candidate: T, score: Double)
    {
		self.candidate = candidate
        self.score = score
    }

    public static func == (a: EvaluatedCandidate, b: EvaluatedCandidate) -> Bool
	{
		return a.score == b.score
    }

    public static func < (a: EvaluatedCandidate, b: EvaluatedCandidate) -> Bool
    {
        return a.score < b.score
    }
}
