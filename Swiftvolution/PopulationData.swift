//
//  PopulationData.swift
//  Swiftvolution
//
//  Created by Jeremy Pereira on 05/09/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//
/// Statistics of a population
open class PopulationData<T>: CustomStringConvertible
{
    private let bestEvaluatedCandidate: EvaluatedCandidate<T>?
    public var bestCandidate: T? { return bestEvaluatedCandidate?.candidate }
    public var bestCandidateFitness: Double? { return bestEvaluatedCandidate?.score }
    public let meanFitness: Double?
    public let fitnessStandardDeviation: Double?
    public let isNatural: Bool
    public let populationCount: Int
    public let eliteCount: Int
    public let generationNumber: Int
    public let timeStamp: Date

    public init(evaluatedPopulation: [EvaluatedCandidate<T>],
                          isNatural: Bool,
                         eliteCount: Int,
                   generationNumber: Int)
    {
        self.timeStamp = Date()
        self.isNatural = isNatural
        self.eliteCount = eliteCount
        self.populationCount = evaluatedPopulation.count
        self.generationNumber = generationNumber

        var totalFitness: Double = 0
        var bestSoFar: EvaluatedCandidate<T>?
        for candidate in evaluatedPopulation
        {
            // First see if we have a new best candidate
			if let previousBestSoFar = bestSoFar
            {
                if (candidate.score > previousBestSoFar.score) == isNatural
                {
                    bestSoFar = candidate
                }
            }
            else
            {
                bestSoFar = candidate
            }
            // Now for the stats stuff
            totalFitness += candidate.score
        }
        self.bestEvaluatedCandidate = bestSoFar
        if populationCount > 0
        {
            self.meanFitness = totalFitness / Double(populationCount)

            // The variance is needed for the standard deviation

            var totalSquareDeviation: Double = 0
            for candidate in evaluatedPopulation
            {
                let deviation = candidate.score - meanFitness!
                totalSquareDeviation += deviation * deviation
            }
            let variance = totalSquareDeviation / Double(populationCount)
            self.fitnessStandardDeviation = sqrt(variance)
        }
        else
        {
            self.meanFitness = nil
            self.fitnessStandardDeviation = nil
        }
    }

    open var description: String
    {
        return populationCount > 0 ? "generation=\(generationNumber) best='\(bestCandidate!)', best fitness=\(bestCandidateFitness!), mean fitness = \(meanFitness!), sd=\(fitnessStandardDeviation!)"
            : "generation=\(generationNumber) extinct"
    }

}
