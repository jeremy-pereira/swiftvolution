//
//  SwiftvolutionTests.swift
//  SwiftvolutionTests
//
//  Created by Jeremy Pereira on 05/08/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import XCTest
import Toolbox
@testable import Swiftvolution

fileprivate struct HelloFactory: CandidateFactory
{
	fileprivate static let chars = Array("ABCDEFGHIJKLMNOPQRSTUVWXYZ ")
	typealias ProduceType =  String

    let target: [Character]

    init(target: String)
    {
        self.target = Array(target)
    }

    func generateRandomCandidate(rng: PRNG) -> String
    {
        var ret = ""

        while ret.utf8.count < target.count
        {
            let randomNumber = rng.zeroBasedRandom(HelloFactory.chars.count)
            ret.append(HelloFactory.chars[randomNumber])
        }
        return ret
    }
}

class SwiftvolutionTests: XCTestCase {

    fileprivate static func evaluateHello(candidate: String, population: [String], target: [Character]) -> Double
    {
        let characters = Array(candidate)
        return zip(characters, target).reduce(0.0)
        {
            (scoreSoFar, charsToCompare) -> Double in
            return scoreSoFar + ((charsToCompare.0 == charsToCompare.1) ? 1 : 0)
        }
    }

    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    
    func testHelloWorld()
    {
        let factory = HelloFactory(target: "HELLO WORLD")
        let rng = Arc4RandomPrng()
        let crossover = StringCrossover()
        let mutate = StringMutation(probability: 0.01, characters: HelloFactory.chars)
        let engine = GenerationalEvolutionEngine<HelloFactory>(
            candidateFactory: factory,
           evolutionOperator: { crossover.apply(parents: $0, rng: $1) } | { mutate.apply(parents: $0, rng: $1) },
           fitnessEvaluator: { SwiftvolutionTests.evaluateHello(candidate: $0, population: $1, target: factory.target) },
           selectionStrategy: {
            (evaluationCandidates, isNatural, finalCount, rng) in
            let top10 = Array(evaluationCandidates[0 ..< 10])
            return SelectionStrategy.rouletteWheel(candidates: top10, isNatural: isNatural, count: finalCount, rng: rng)
        },
           			   rng: rng)
        let answer = engine.evolve(populationCount: 10, eliteCount: 0, seeds: [])
        {
            (stats) -> Bool in
            if let bestFitness = stats.bestCandidateFitness
            {
                return bestFitness > Double(factory.target.count - 1)
            }
            else
            {
                return true
            }
        }
        print(answer ?? "Extinct!")
    }

    func testObserver()
    {
        let factory = HelloFactory(target: "HELLO WORLD")
        let rng = Arc4RandomPrng()
        let crossover = StringCrossover()
        let mutate = StringMutation(probability: 0.01, characters: HelloFactory.chars)
        var engine = GenerationalEvolutionEngine<HelloFactory>(
            candidateFactory: factory,
            evolutionOperator: { crossover.apply(parents: $0, rng: $1) } | { mutate.apply(parents: $0, rng: $1) },
            fitnessEvaluator: { SwiftvolutionTests.evaluateHello(candidate: $0, population: $1, target: factory.target) },
            selectionStrategy: {
                (evaluationCandidates, isNatural, finalCount, rng) in
                let top10 = Array(evaluationCandidates[0 ..< 10])
                return SelectionStrategy.rouletteWheel(candidates: top10, isNatural: isNatural, count: finalCount, rng: rng)
        },
            rng: rng)
        engine.addObserver
        {
            print($0)
        }
        let answer = engine.evolve(populationCount: 10, eliteCount: 0, seeds: [])
        {
            (stats) -> Bool in
            if let bestFitness = stats.bestCandidateFitness
            {
                return bestFitness > Double(factory.target.count - 1)
            }
            else
            {
                return true
            }
        }
        print(answer ?? "Extinct!")
    }


    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
