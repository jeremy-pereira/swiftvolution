//
//  MaleFemaleTests.swift
//  Swiftvolution
//
//  Created by Jeremy Pereira on 09/09/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import XCTest
import Toolbox
import Swiftvolution


/// The thing we are going to evolve. The model is based on human sexes. 
/// Females produce eggs with an X, males produce sperm with either an X or a Y
/// We add a gene that dictates the proportion of X's and Y's the male produces.
/// Organisms are inherently female. We have a subclass to reresent males
///
class Organism: CustomStringConvertible
{
    static var mutationRate: Double = 0.02

    let gene: [Bool]
    let litterSize: Int

    var isFemale: Bool { return true }

    init(gene: [Bool], litterSize: Int)
    {
        self.gene = gene
        self.litterSize = litterSize
    }

    func mate(other: Organism,rng: PRNG) -> [Organism]
    {
        var ret: [Organism] = []
        guard other.isFemale != self.isFemale else { return [] }
        guard self.isFemale else { return other.mate(other: self, rng: rng) }
        // Now we have ourself: a female and other: a male
        let otherGene = other.gene
        for _ in 0 ..< litterSize
        {
			let willBeFemale = otherGene[rng.zeroBasedRandom(otherGene.count)]
            let (newGene1, newGene2) = simpleCrossover(a: self.gene, b: otherGene, rng: rng)
            let selectedGene = singleMutation(gene: rng.select(0.5) ? newGene1 : newGene2,
                                         mutations: [true, false],
                                       probability: Organism.mutationRate,
                                       		   rng: rng)

            let child = willBeFemale ? Organism(gene: selectedGene, litterSize: litterSize)
                                     : Organism.Male(gene: selectedGene, litterSize: litterSize)
			ret.append(child)
        }
        return ret
    }

    var description: String
    {
        return (isFemale ? "Female: " : "Male: ") + gene.reduce("", {
            (string, isFemale) in
            return string + (isFemale ? "X" : "Y")
        })
    }
}

extension Organism
{
    class Male: Organism
    {
        override var isFemale: Bool { return false }
    }
}

struct OrganismFactory: CandidateFactory
{
    let maleProbability: Double
    let geneLength: Int
    let litterSize: Int

    init(maleProbability: Double, geneLength: Int, litterSize: Int)
    {
        self.maleProbability = maleProbability
        self.geneLength = geneLength
        self.litterSize = litterSize
    }

    func generateRandomCandidate(rng: PRNG) -> Organism
    {
        var gene: [Bool] = []
        let ret: Organism

        for _ in 0 ..< geneLength
        {
            gene.append(rng.select(1 - maleProbability))
        }
        if rng.select(maleProbability)
        {
            ret = Organism.Male(gene: gene, litterSize: litterSize)
        }
        else
        {
            ret = Organism(gene: gene, litterSize: litterSize)
        }
        return ret
    }
}

fileprivate struct Breeder
{
    private let malesToBreed: Int
    private let femalesPerMale: Int
    private let childrenPerFemale: Int

    fileprivate init(malesToBreed: Int, femalesPerMale: Int, childrenPerFemale: Int)
    {
		self.malesToBreed = malesToBreed
        self.femalesPerMale = femalesPerMale
        self.childrenPerFemale = childrenPerFemale
    }

    fileprivate func breed(organisms: ([Organism], [Organism]), rng: PRNG) -> [Organism]
    {
        let (females, males) = organisms
        guard males.count > 0 && females.count > 0 else { return [] }
        var ret: [Organism] = []
        for _ in 0 ..< malesToBreed
        {
            let theMale = males[rng.zeroBasedRandom(males.count)]
            for _ in 0 ..< femalesPerMale
            {
                let theFemale = females[rng.zeroBasedRandom(females.count)]
                ret += theFemale.mate(other: theMale, rng: rng)
            }
        }
        return ret
	}
}

class MaleFemaleData: PopulationData<Organism>
{
    let maleCount: Int
    let femaleCount: Int
    override init(evaluatedPopulation: [EvaluatedCandidate<Organism>], isNatural: Bool, eliteCount: Int, generationNumber: Int)
    {
        (maleCount, femaleCount) = evaluatedPopulation.reduce((0, 0))
        {
            (runningTotal, evaluatedOrganism) -> (Int, Int) in
            let runningMaleCount = runningTotal.0 + (evaluatedOrganism.candidate.isFemale ? 0 : 1)
            let runningFemaleCount = runningTotal.1 + (evaluatedOrganism.candidate.isFemale ? 1 : 0)
            return (runningMaleCount, runningFemaleCount)
        }
        super.init(evaluatedPopulation: evaluatedPopulation, isNatural: isNatural, eliteCount: eliteCount, generationNumber: generationNumber)
    }
	override var description: String
    {
		return "generation=\(generationNumber) best='\(bestCandidate!)' males=\(maleCount), females=\(femaleCount)"
    }
}

class MaleFemaleTests: XCTestCase
{
    static func separateMalesAndFemales(organisms: [Organism], rng: PRNG) -> ([Organism], [Organism])
    {
        var males: [Organism] = []
        var females: [Organism] = []

        for organism in organisms
        {
            if organism.isFemale
            {
                females.append(organism)
            }
            else
            {
                males.append(organism)
            }
        }
        return (females, males)
    }

    override func setUp()
    {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown()
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testMaleFemale()
    {
        let factory = OrganismFactory(maleProbability: 0.2, geneLength: 20, litterSize: 10)
        let rng = Arc4RandomPrng()

        let breeder = Breeder(malesToBreed: 5, femalesPerMale: 5, childrenPerFemale: 5)
        var engine = GenerationalEvolutionEngine<OrganismFactory>(
             candidateFactory: factory,
             evolutionOperator: MaleFemaleTests.separateMalesAndFemales | breeder.breed,
            fitnessEvaluator: { (individual, _) in return individual.isFemale ? 1.0 : 1.0 },
            selectionStrategy: SelectionStrategy.rouletteWheel,
            			  rng: rng)
        engine.addObserver
        {
            print($0)
        }
        engine.populationDataFactory = {
            (evaluatedCandidates, isNatural, eliteCount, generationNumber) in
            return MaleFemaleData(evaluatedPopulation: evaluatedCandidates,
                                            isNatural: isNatural,
                                           eliteCount: eliteCount,
                                     generationNumber: generationNumber)
        }
        let answer = engine.evolve(populationCount: 100, eliteCount: 0, seeds: [])
        {
            (stats) -> Bool in
            return stats.generationNumber > 1000
        }
        if let answer = answer
        {
            print(answer)
        }
        else
        {
            print("Extinct!")
        }
    }
}
