# README #

### What is this repository for? ###

This is the swiftvolution evolution engine. It's written entirely in Swift and can model evolution in an extensible way.

The engine is based on the [Watchmaker Framework](https://watchmaker.uncommons.org/) which was implemented in Java, 
has a good set of API documentation and a nice tutorial.

My implementation remodels the APIs and some of the classes in Swift with minor changes to take advantage of Swift features.

The implementation is almost completely my own, although I would be lying if I said that I never looked at the Watchmaker 
source code. For this reason, I am using the same Apache 2.0 licence.

### How do I get set up? ###

The project has no dependencies except for my Toolbox framework. Git clone this project and Toolbox into the same directory, 
add them both to an Xcode 8.3.3 workspace and you should be good to go.

Further information can be found at https://sincereflattery.blog/